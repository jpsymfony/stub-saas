<?php

namespace App\Controller\Admin;

use App\Controller\Admin\AdminBaseController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

use App\Entity\User;
use App\Entity\Account;
use App\Entity\ApiKey;

/** 
 * @Route("/admin/api", name="admin_api_") 
 */
class ApiController extends AdminBaseController
{
    /**
     * @Route("/list", name="list")
     */
    public function list(Request $request)
    {
        $repo       = $this->getDoctrine()->getRepository(ApiKey::class);
        $keys       = $repo->findAll();
        
        return $this->render('Admin/api/list.html.twig', [
            'keys' => $keys,
        ]);
    }
}