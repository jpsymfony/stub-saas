<?php

namespace App\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use App\Service\MailerService;

use Ramsey\Uuid\Uuid;
use App\Entity\User;

class SecurityController extends AbstractController
{
    private $params; 
    private $mailService;

    public function __construct(MailerService $mailService, ParameterBagInterface $params)
    {
        $this->params = $params;
        $this->mailService = $mailService;
    }

    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        if ($this->getUser()) {
            return $this->redirectToRoute('account_dashboard');
        }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last email entered by the user
        $lastEmail = $authenticationUtils->getLastUsername();

        return $this->render('Web/Security/login.html.twig', [
            'last_email' => $lastEmail, 
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/password/reset", name="app_resetpass")
     */
    public function lostPassword(Request $request)
    {
        $post = false;
        if ($request->isMethod('POST')) {
            $email = $request->request->get('email');
            $ttl  = $this->params->get('password_token_ttl');

            $repo = $this->getDoctrine()->getRepository(User::class);
            $user = $repo->findEmail($email);

            if ($user instanceof User) {
                $token = Uuid::uuid4();
                $user->setPasswordToken($token);

                $date = new \DateTime();
                $date->add(new \DateInterval('PT'.$ttl.'H'));
                $user->setTokenExpiresAt($date);
                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();

                ## Send user the lost password email
                $this->mailService->sendForgotPassword($user);
            }

            $post = true;
        }

        return $this->render('Web/Security/forgotPassword.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/password/change/{token}", name="app_changepass", methods={"GET", "POST"})
     */
    public function changePassword($token, Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {
        $repo   = $this->getDoctrine()->getRepository(User::class);
        $user   = $repo->findToken($token, $this->params->get('password_token_ttl'));
        
        if (!$user instanceof User) {
            return $this->render('Web/Security/tokenInvalid.html.twig');
        }

        $showForm = true;

        if ($request->isMethod("POST")) {
            $params = $request->request->all();
            $pass1  = isset($params['pass1']) ? trim($params['pass1']) : null;
            $pass2  = isset($params['pass2']) ? trim($params['pass2']) : null;
            
            if (empty($pass1) || empty($pass2) || $pass1 !== $pass2) {
                $this->addFlash('danger', 'Your passwords do not match');
            } else {
                // See .env and config/packages/services.yml for settings
                $minLength      = $this->params->get('password_min_length');
                $uppercase      = true;
                $lowercase      = true;
                $number         = true;
                $specialChars   = true;
                // Password complexity settings - require at least 1 of each
                if ($this->params->get('require_password_uppercase')) {
                    $uppercase      = preg_match('@[A-Z]@', $pass1);
                }
                if ($this->params->get('require_password_lowercase')) {
                    $lowercase      = preg_match('@[a-z]@', $pass1);
                }
                if ($this->params->get('require_password_number')) {
                    $number         = preg_match('@[0-9]@', $pass1);
                }
                if ($this->params->get('require_password_specialchars')) {
                    $specialChars   = preg_match('@[^\w]@', $pass1);
                }
                if (!$uppercase || !$lowercase || !$number || !$specialChars || \mb_strlen($pass1) < $minLength) {
                    $this->addFlash('danger', 'Your password should contain at least 1 uppercase, 1 lowercase, 1 numeral, 1 special character, & be '.$minLength.' characters long.');
                } else {
                    $user->setTokenExpiresAt(null);
                    $user->setPasswordToken(null);            
                    $user->setPassword($passwordEncoder->encodePassword($user, $pass1));
                    
                    // For a new user this will activate their account and allow login
                    // Provided they haven't been marked inactive; 
                    $user->setIsVerified(true);

                    $this->getDoctrine()->getManager()->persist($user);
                    $this->getDoctrine()->getManager()->flush();

                    $this->addFlash('success', 'Your password has been successfully updated');
                    $showForm = false;
                }
            }     
        }

        return $this->render('Web/Security/changePassword.html.twig', [
            'id'        => $user->getId(),
            'showForm'  => $showForm,
        ]);
    }

    /**
     * @Route("/signup", name="app_signup", methods={"GET","POST"})
     */
    public function signup(Request $request)
    {
        // Setting is controlled in .env 
        $allowSignUp = $this->getParameter('allow_self_register');
        if ($allowSignUp == 'false') {
            return $this->redirectToRoute('app_login');
        }

        if ($request->isMethod('POST')) {
            // TODO Impliment sign-up process
            // Make sure you have an email service to send emails
        }

        return $this->render('Web/Security/signUp.html.twig');
    }
}
