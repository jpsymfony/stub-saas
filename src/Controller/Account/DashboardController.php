<?php

namespace App\Controller\Account;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/** 
 * @Route("/account", name="account_") 
 */
class DashboardController extends AbstractController
{
    /**
     * @Route("/dashboard", name="dashboard")
     */
    public function index()
    {
        
        return $this->render('Account/dashboard/index.html.twig', [
            'controller_name' => 'DashboardController',
        ]);
    }
}
