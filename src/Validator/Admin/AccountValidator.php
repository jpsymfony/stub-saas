<?php

namespace App\Validator\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Entity\User;
use App\Entity\Account;

class AccountValidator 
{
    private $em;
    private $accountRepo;
    private $params;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $params)
    {
        $this->params       = $params;
        $this->em           = $em;
        $this->accountRepo  = $this->em->getRepository(\App\Entity\Account::class);
    }


    /**
     * @param Account $account
     * @param array $array
     * @return array
     */
    public function accountValidate(Account $account, array $data): array
    {
        $errors         = [];
        $name           = isset($data['name'])           ? trim($data['name']) : null;
        $website        = isset($data['website'])        ? trim($data['website']) : null;
        $allowedDomains = isset($data['allowedDomains']) ? trim($data['allowedDomains']) : null;
        
        if (empty($name)) {
            $errors[] = ['danger' => 'A name is required to create a new account'];
        } else {
            $account->setName($name);
        }

        $account->setWebsite($website);
        
        if (!empty($allowedDomains)) {
            if (strstr($allowedDomains, ',')) {
                $l = explode(',', $allowedDomains);
                $a = null;
                $i = 0;
                foreach ($l as $d) {
                    if ($i == 0) {
                        $a.= trim($d);
                    } else {
                        $a.= ',' . trim($d);
                    }
                    $i++;
                }
                $allowedDomains = $a;
            
            }
        }
        
        $account->setAllowedDomains($allowedDomains);
        
        return [
            'errors'    => $errors,
            'account'   => $account
        ];
    }
}