<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Ramsey\Uuid\Doctrine\UuidGenerator;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
     /**
      * @var \Ramsey\Uuid\UuidInterface
      *
      * @ORM\Id()
      * @ORM\Column(type="uuid", unique=true)
      * @ORM\GeneratedValue(strategy="CUSTOM")
      * @ORM\CustomIdGenerator(class=UuidGenerator::class)
      */
    private $id;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     */
    #private $username;

    /**
     * @ORM\Column(type="json")
     */
    private $roles = [];

    /**
     * @var string The hashed password
     * @ORM\Column(type="string")
     */
    private $password;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isActive;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDeleted;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $lastLoginAt;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity=Account::class, inversedBy="users")
     * @ORM\JoinColumn(nullable=false)
     */
    private $account;

    /**
     * @ORM\Column(type="string", length=50, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isVerified;

    /**
     * @ORM\Column(type="string", length=36, nullable=true)
     */
    private $passwordToken;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $tokenExpiresAt;

    /**
     * @ORM\OneToMany(targetEntity=UserNote::class, mappedBy="createdBy")
     */
    private $createdByNotes;

    /**
     * @ORM\OneToMany(targetEntity=UserNote::class, mappedBy="user")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     */
    private $userNotes;

    /**
     * @ORM\Column(type="string", length=20, nullable=true)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=25, nullable=true)
     */
    private $lastName;

    /**
     * @ORM\OneToMany(targetEntity=AccountNote::class, mappedBy="createdBy", orphanRemoval=true)
     */
    private $accountNotes;

    /**
     * @ORM\OneToMany(targetEntity=ApiKey::class, mappedBy="user", orphanRemoval=true)
     */
    private $apiKeys;

    public function __construct()
    {
        $this->createdAt        = new \DateTime();
        $this->isVerified       = false;
        $this->isDeleted        = false;
        $this->createdByNotes   = new ArrayCollection();
        $this->userNotes        = new ArrayCollection();
        $this->accountNotes     = new ArrayCollection();
        $this->apiKeys          = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * A visual identifier that represents this user.
     *
     * @see UserInterface
     */
    public function getUsername(): string
    {
        return (string) $this->email;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;
        
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @see UserInterface
     */
    public function getSalt()
    {
        // not needed when using the "bcrypt" algorithm in security.yaml
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials()
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    public function getIsActive(): ?bool
    {
        return $this->isActive;
    }

    public function setIsActive(bool $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    public function getIsDeleted(): ?bool
    {
        return $this->isDeleted;
    }

    public function setIsDeleted(bool $isDeleted): self
    {
        $this->isDeleted = $isDeleted;

        return $this;
    }

    public function getLastLoginAt(): ?\DateTimeInterface
    {
        return $this->lastLoginAt;
    }

    public function setLastLoginAt(?\DateTimeInterface $lastLoginAt): self
    {
        $this->lastLoginAt = $lastLoginAt;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getAccount(): ?account
    {
        return $this->account;
    }

    public function setAccount(?Account $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getIsVerified(): ?bool
    {
        return $this->isVerified;
    }

    public function setIsVerified(?bool $isVerified): self
    {
        $this->isVerified = $isVerified;

        return $this;
    }

    public function getPasswordToken(): ?string
    {
        return $this->passwordToken;
    }

    public function setPasswordToken(?string $passwordToken): self
    {
        $this->passwordToken = $passwordToken;

        return $this;
    }

    public function getTokenExpiresAt(): ?\DateTimeInterface
    {
        return $this->tokenExpiresAt;
    }

    public function setTokenExpiresAt(?\DateTimeInterface $tokenExpiresAt): self
    {
        $this->tokenExpiresAt = $tokenExpiresAt;

        return $this;
    }

    /**
     * @return Collection|UserNote[]
     */
    public function getCreatedByNotes(): Collection
    {
        return $this->createdByNotes;
    }

    public function addCreatedByNote(UserNote $createdByNote): self
    {
        if (!$this->createdByNotes->contains($createdByNote)) {
            $this->createdByNotes[] = $createdByNote;
            $createdByNote->setCreatedBy($this);
        }

        return $this;
    }

    public function removeCreatedByNote(UserNote $createdByNote): self
    {
        if ($this->createdByNotes->contains($createdByNote)) {
            $this->createdByNotes->removeElement($createdByNote);
            // set the owning side to null (unless already changed)
            if ($createdByNote->getCreatedBy() === $this) {
                $createdByNote->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|UserNote[]
     */
    public function getUserNotes(): Collection
    {
        return $this->userNotes;
    }

    public function addUserNote(UserNote $userNote): self
    {
        if (!$this->userNotes->contains($userNote)) {
            $this->userNotes[] = $userNote;
            $userNote->setUser($this);
        }

        return $this;
    }

    public function removeUserNote(UserNote $userNote): self
    {
        if ($this->userNotes->contains($userNote)) {
            $this->userNotes->removeElement($userNote);
            // set the owning side to null (unless already changed)
            if ($userNote->getUser() === $this) {
                $userNote->setUser(null);
            }
        }

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * @return Collection|AccountNote[]
     */
    public function getAccountNotes(): Collection
    {
        return $this->accountNotes;
    }

    public function addAccountNote(AccountNote $accountNote): self
    {
        if (!$this->accountNotes->contains($accountNote)) {
            $this->accountNotes[] = $accountNote;
            $accountNote->setCreatedBy($this);
        }

        return $this;
    }

    public function removeAccountNote(AccountNote $accountNote): self
    {
        if ($this->accountNotes->contains($accountNote)) {
            $this->accountNotes->removeElement($accountNote);
            // set the owning side to null (unless already changed)
            if ($accountNote->getCreatedBy() === $this) {
                $accountNote->setCreatedBy(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|ApiKey[]
     */
    public function getApiKeys(): Collection
    {
        return $this->apiKeys;
    }

    public function addApiKey(ApiKey $apiKey): self
    {
        if (!$this->apiKeys->contains($apiKey)) {
            $this->apiKeys[] = $apiKey;
            $apiKey->setUser($this);
        }

        return $this;
    }

    public function removeApiKey(ApiKey $apiKey): self
    {
        if ($this->apiKeys->contains($apiKey)) {
            $this->apiKeys->removeElement($apiKey);
            // set the owning side to null (unless already changed)
            if ($apiKey->getUser() === $this) {
                $apiKey->setUser(null);
            }
        }

        return $this;
    }

  
}
