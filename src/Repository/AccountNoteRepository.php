<?php

namespace App\Repository;

use App\Entity\AccountNote;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method AccountNote|null find($id, $lockMode = null, $lockVersion = null)
 * @method AccountNote|null findOneBy(array $criteria, array $orderBy = null)
 * @method AccountNote[]    findAll()
 * @method AccountNote[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AccountNoteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AccountNote::class);
    }

    // /**
    //  * @return AccountNote[] Returns an array of AccountNote objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?AccountNote
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
